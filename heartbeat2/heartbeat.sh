#!/bin/bash

#Declarando as variaveis dos comandos informando o diretório de origem.
IP="/usr/sbin/ip"
ADDRESS="/usr/sbin/ip"
ADD="/usr/sbin/ip"
DEV="/usr/sbin/ip"
PING="/bin/ping"
ECHO="/bin/echo"
IFCONFIG="/sbin/ifconfig"


#Declarando a função para adicionar o IP na placa ETH0
function SEOFFADD { 
	$IP address add "IP_DA_MAQUINA_PRINCIPAL-1" dev eth0 
}

#Removendo o IP antigo da placa ETH0 para ficar o novo IP
function SEOFFDEL {
	$IP address del "IP_DA_MAQUINA_ESCRAVA" dev eth0
}


#Quando a máquina principal voltar para a rede, a função SEONADD adiciona um novo IP a placa ETH0 
function SEONADD {
	$IP address add "IP_DA_MAQUINA_ESCRAVA" dev eth0
}

#Deleta O IP pois a máquina principal voltou a rede
function SEONDEL {
	$IP address del "IP_DAMAQUINA_PRINCIPAL-1" dev eth0
}

#Disparando o PING para validar se a máquina principal está na rede
IP2=$(ping -c1 "IP_DA_MAQUINA_PRINCIPAL-2" | grep From | awk -F' ' '{ print $4 $5 $6}')


#Realiza a verificação se o IP da máquina principal está na rede.
if [ "$IP2" == "DestinationHostUnreachable" ];
then
        $ECHO "offline!!!"
	SEOFFADD 
	SEOFFDEL	

else
       
	$ECHO "online!!!"
	SEONADD
	SEONDEL
fi

