# heartbeat

Essa aplicação tem a função de auxiliar na implantação de alta disponibilidade, tendo como função verificar se a maquina principal está online.

Para utilização do script é necessário que o servidor principal tenha 2 IPs distintos.

O shell deve ser executado pela maquina escrava, a execução pode ser automatizada adicionando uma rotina ao crontab ou aplicando um loop no script e executando o mesmo em background durante a inicialização do sistema.

A maquina escrava verificará se a maquina principal está online atravez do protocolo icmp, se por algum motivo a maquina principal parar de responder o icmp a maquina escrava assumirá o IP da maquina principal, tomando para si as suas funcões.

Uma vez que a maquina principal volte a responder as requisições de icmp a maquina escrava voltara a assumir seu IP de origem

Para utilização da aplicação basta apenas configurá-la com os IPs de cada maquina.
